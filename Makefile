pkg_example=packages/example
pkg_example_dist=$(pkg_example)/dist
pkg_example_src=$(pkg_example)/src

pkg_webide=packages/web-ide
pkg_webide_src=$(pkg_webide)/src
pkg_webide_lib=$(pkg_webide)/lib
pkg_webide_dist=$(pkg_webide)/dist
pkg_webide_dist_public=$(pkg_webide_dist)/public
# TODO: Depend on .touch files instead of sources of a dir
pkg_webide_dist_public_sources=\
 $(pkg_webide_dist_public)/main.js \
 $(pkg_webide_dist_public)/vscode \
 $(pkg_webide_dist_public)/vscode/extensions/gitlab-web-ide

pkg_vscode_bootstrap=packages/vscode-bootstrap
pkg_vscode_bootstrap_dist=$(pkg_vscode_bootstrap)/dist

pkg_vscode_build=packages/vscode-build
pkg_vscode_build_dist=$(pkg_vscode_build)/dist

pkg_vscode_extension=packages/web-ide-vscode-extension
pkg_vscode_extension_dist=$(pkg_vscode_extension)/dist
pkg_vscode_extension_dist_sources=\
 $(pkg_vscode_extension_dist)/main.js \
 $(pkg_vscode_extension_dist)/package.json \
 $(pkg_vscode_extension_dist)/package.nls.json

all_ts=$(shell find packages/ -type f -name '*.ts' -path 'packages/*/src/*' -not -path '*/dist/*')
all_html=$(shell find packages/ -type f -name '*.html' -path 'packages/*/src/*' -not -path '*/dist/*')
all_src=$(all_ts) $(all_html)

## =======
## web-ide
## =======

list:
	@echo all_ts = $(all_ts)
	@echo ""
	@echo all_html = $(all_html)
	@echo ""
	@echo all_src = $(all_src)

$(pkg_webide): $(pkg_webide_dist) $(pkg_webide_lib)

$(pkg_webide_dist): $(pkg_webide_dist_public)

$(pkg_webide_lib): $(pkg_webide_src)/*
	yarn build:ts $(pkg_webide)

$(pkg_webide_dist_public): $(pkg_webide_dist_public_sources)

$(pkg_webide_dist_public)/main.js: $(pkg_vscode_bootstrap_dist)/main.js
	rm -f $@
	mkdir -p $(dir $@)
	cp $< $@

$(pkg_webide_dist_public)/vscode: $(pkg_vscode_build_dist)/vscode
	rm -rf $@
	mkdir -p $(dir $@)
	cp -r $< $@

$(pkg_webide_dist_public)/vscode/extensions/gitlab-web-ide: $(pkg_vscode_extension_dist_sources)
	rm -rf $@
	mkdir -p $@
	cp -r $(pkg_vscode_extension_dist)/* $@

## ================
## vscode-bootstrap
## ================

$(pkg_vscode_bootstrap_dist)/main.js: $(all_ts)
	yarn workspace @gitlab/vscode-bootstrap run build

## ============
## vscode-build
## ============

# what: Let's force this to always run the `yarn workspace` script.
# why: This way `packages/vscode-build` can own whether it needs to rerun or not.
$(pkg_vscode_build_dist)/vscode: FORCE
	yarn workspace @gitlab/vscode-build run build

## =======
## example
## =======
$(pkg_example): $(pkg_example_dist)

$(pkg_example_dist): \
 $(pkg_example_dist)/index.html \
 $(pkg_example_dist)/index.js \
 $(pkg_example_dist)/web-ide/public

$(pkg_example_dist)/index.html: $(pkg_example_src)/index.html
	rm -f $@
	mkdir -p $(dir $@)
	cp -r $< $@

$(pkg_example_dist)/index.js: $(all_src) $(pkg_example)/package.json
	rm -f $@
	yarn build:ts
	yarn workspace @gitlab/example run build

$(pkg_example_dist)/web-ide/public: $(pkg_webide_dist_public_sources)
	rm -rf $@
	mkdir -p $(dir $@)
	cp -r $(pkg_webide_dist_public) $@

## ========================
## web-ide-vscode-extension
## ========================
$(pkg_vscode_extension): $(pkg_vscode_extension_dist)

$(pkg_vscode_extension_dist): $(pkg_vscode_extension_dist_sources)

$(pkg_vscode_extension_dist)/main.js: $(all_ts)
	rm -f $@
	yarn workspace @gitlab/web-ide-vscode-extension run build

$(pkg_vscode_extension_dist)/%.json: $(pkg_vscode_extension)/vscode.%.json
	rm -f $@
	mkdir -p $(dir $@)
	cp -r $< $@

# what: https://www.gnu.org/software/make/manual/html_node/Force-Targets.html
FORCE:
