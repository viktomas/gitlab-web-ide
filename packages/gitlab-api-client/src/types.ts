/* eslint-disable-next-line @typescript-eslint/no-namespace */
export namespace gitlab {
  export interface Commit {
    id: string;
    short_id: string;
    created_at: string;
    title: string;
    message: string;
    web_url: string;
  }

  export interface Branch {
    name: string;
    commit: Commit;
    web_url: string;
  }

  export interface File {
    id: string;
    last_commit_sha: string;
    path: string;
    name: string;
    extension: string;
    size: number;
    mime_type: string;
    binary: boolean;
    simple_viewer: string;
    rich_viewer: string;
    show_viewer_switcher: string;
    render_error?: string;
    raw_path: string;
    blame_path: string;
    commits_path: string;
    tree_path: string;
    permalink: string;
  }

  // https://docs.gitlab.com/ee/api/repositories.html#list-repository-tree
  export interface RepositoryTreeItem {
    id: string;
    name: string;
    type: 'tree' | 'blob';
    path: string;
    mode: string;
  }
}
