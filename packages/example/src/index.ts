import { start } from '@gitlab/web-ide';
import { getBaseUrlFromLocation } from './config';

// types
// =====

interface UserConfig {
  gitlabUrl: string;
  projectPath: string;
  ref: string;
}

// constants
// =========

const SEL_MAIN = '.main';

const INPUT_GITLAB_URL = 'gitlab_url';
const INPUT_PROJECT_PATH = 'project_path';
const INPUT_REF = 'ref';

const STORAGE_KEY = 'gitlab.web-ide-example.config';

const DEFAULT_CONFIG: UserConfig = {
  gitlabUrl: 'https://gitlab.com/',
  projectPath: 'gitlab-org/gitlab',
  ref: 'master',
};

// utils
// =====

const getFormDataAsString = (formData: FormData, key: string): string =>
  formData.get(key)?.toString() || '';

const getUserConfigFromForm = (form: HTMLFormElement): UserConfig => {
  const formData = new FormData(form);

  return {
    gitlabUrl: getFormDataAsString(formData, INPUT_GITLAB_URL),
    projectPath: getFormDataAsString(formData, INPUT_PROJECT_PATH),
    ref: getFormDataAsString(formData, INPUT_REF),
  };
};

const saveUserConfig = (config: UserConfig): void => {
  try {
    localStorage.setItem(STORAGE_KEY, JSON.stringify(config));
  } catch (e) {
    // eslint-disable-next-line no-console
    console.error('[gitlab-web-ide-example] Could not save user config!', e);
  }
};

const loadUserConfig = (): UserConfig | null => {
  const savedJson = localStorage.getItem(STORAGE_KEY);

  if (!savedJson) {
    return null;
  }

  try {
    const savedObj = JSON.parse(savedJson);

    if (typeof savedObj === 'object') {
      return savedObj as UserConfig;
    }
    return null;
  } catch {
    return null;
  }
};

const setFormValue = (form: HTMLFormElement, name: string, value: string) => {
  const el = form.querySelector(`[name="${name}"]`) as HTMLFormElement | null;

  if (!el) {
    return;
  }

  el.value = value;
};

const setupConfigForm = (form: HTMLFormElement, config: UserConfig) => {
  setFormValue(form, INPUT_GITLAB_URL, config.gitlabUrl);
  setFormValue(form, INPUT_PROJECT_PATH, config.projectPath);
  setFormValue(form, INPUT_REF, config.ref);
};

const waitForUserConfig = (form: HTMLFormElement): Promise<UserConfig> => {
  const savedConfig = loadUserConfig();

  const config: UserConfig = {
    ...DEFAULT_CONFIG,
    ...(savedConfig || {}),
  };

  setupConfigForm(form, config);

  return new Promise(resolve => {
    form.addEventListener('submit', e => {
      e.preventDefault();

      const userConfig = getUserConfigFromForm(form);

      saveUserConfig(userConfig);

      resolve(userConfig);
    });
  });
};

const die = (msg: string) => {
  // eslint-disable-next-line no-alert
  alert(`Something bad happened while starting the example! Message:\n${msg}`);

  return new Error(msg);
};

// main
// ====

const main = async () => {
  const container = document.querySelector(SEL_MAIN);

  if (!container) {
    throw die(`Could not find element "${SEL_MAIN}"!`);
  }

  const form = container.querySelector('form');

  if (!form) {
    throw die('Could not find form!');
  }

  const userConfig = await waitForUserConfig(form);

  container.innerHTML = '';

  await start(container, {
    baseUrl: getBaseUrlFromLocation(),
    gitlabToken: '',
    ...userConfig,
  });
};

// eslint-disable-next-line @typescript-eslint/no-floating-promises
main();
