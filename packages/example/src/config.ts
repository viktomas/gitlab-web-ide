export const getBaseUrlFromLocation = () => {
  // eslint-disable-next-line no-restricted-globals
  const newUrl = new URL('web-ide/public', location.href);

  return newUrl.href;
};
