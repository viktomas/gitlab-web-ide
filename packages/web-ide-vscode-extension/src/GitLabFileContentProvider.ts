import { IFileContentProvider } from '@gitlab/web-ide-fs';
import { fetchFileRaw } from './mediator/commands';

export class GitLabFileContentProvider implements IFileContentProvider {
  private readonly _ref: string;

  constructor(ref: string) {
    this._ref = ref;
  }

  async getContent(path: string): Promise<Uint8Array> {
    const vsbuffer = await fetchFileRaw(this._ref, path);

    return vsbuffer.buffer;
  }
}
