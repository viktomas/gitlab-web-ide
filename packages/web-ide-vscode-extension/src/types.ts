interface UnloadedContent {
  type: 'unloaded';
  path: string;
}

interface RawContent {
  type: 'raw';
  raw: Uint8Array;
}

export interface FileEntry {
  readonly type: 'blob' | 'tree';
  readonly name: string;
  readonly ctime: number;
  readonly mtime: number;
  readonly size: number;
  readonly children?: string[];
  readonly content?: UnloadedContent | RawContent;
  // The hash of the initially loaded content
  readonly initContentHash?: string;
}

export interface MutableFileEntry extends FileEntry {
  ctime: number;
  mtime: number;
  size: number;
  children?: string[];
  content?: UnloadedContent | RawContent;
  initContentHash?: string;
}

export interface IFileSystem {
  getEntry(path: string): FileEntry | undefined;
  getParentEntry(path: string): FileEntry | undefined;
  writeFile(path: string, content: Uint8Array): void;
  saveLoadedContent(path: string, content: Uint8Array): FileEntry;
  createFile(path: string, content?: Uint8Array): void;
  createDirectory(path: string): void;
  delete(path: string): void;
  rename(oldPath: string, newPath: string): void;
  copy(sourcePath: string, targetPath: string): void;
}

export interface IFileSearcher {
  searchBlobPaths(term: string, maxResults?: number): Promise<string[]>;
}

export enum FileStatusType {
  Modified = 1,
  Created = 2,
  Deleted = 3,
}

export interface IFileStatus {
  readonly path: string;
  readonly type: FileStatusType;
  readonly origObjId?: string;
  readonly newObjId?: string;
  readonly origContentKey?: string;
}

export interface IGit {
  // TODO: We can status different heads
  status(): IFileStatus[];
  init(fs: IFileSystem): void;
  update(fs: IFileSystem): void;
  clean(): void;
}
