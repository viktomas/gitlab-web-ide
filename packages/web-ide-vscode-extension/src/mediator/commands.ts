import * as vscode from 'vscode';
import {
  COMMAND_START,
  COMMAND_FETCH_FILE_RAW,
  COMMAND_READY,
  StartCommandResponse,
  VSCodeBuffer,
} from '@gitlab/vscode-mediator-commands';

export const start = (): Thenable<StartCommandResponse> =>
  vscode.commands.executeCommand(COMMAND_START);

export const fetchFileRaw = (ref: string, path: string): Thenable<VSCodeBuffer> =>
  vscode.commands.executeCommand(COMMAND_FETCH_FILE_RAW, ref, path);

export const ready = () => vscode.commands.executeCommand(COMMAND_READY);
