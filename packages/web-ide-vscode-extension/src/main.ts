import * as vscode from 'vscode';
import { createSystems, FileList } from '@gitlab/web-ide-fs';
import { GitLabFileSearchProvider } from './vscode/GitLabFileSearchProvider';
import { start, ready } from './mediator/commands';
import { GitLabFileContentProvider } from './GitLabFileContentProvider';
import { FileSearcher } from './FileSearcher';
import { GitLabFileSystemProvider } from './vscode/GitLabFileSystemProvider';

const fetchContextTask =
  (
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    context: vscode.ExtensionContext,
  ) =>
  async (
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    progress: vscode.Progress<{ increment: number; message: string }>,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    token: vscode.CancellationToken,
  ) => {
    const result = await start();

    // New BrowserFS File System!!!
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { fs, sourceControl } = await createSystems({
      contentProvider: new GitLabFileContentProvider(result.branch.commit.id),
      gitLsTree: result.files,
      repoRoot: result.repoRoot,
    });
    const fileList = new FileList(fs, result.repoRoot).withCache();
    const vscodeFs = new GitLabFileSystemProvider(fs);

    vscode.workspace.registerFileSystemProvider('gitlab-vscode-ide', vscodeFs, {
      isCaseSensitive: true,
      isReadonly: false,
    });
    vscode.workspace.registerFileSearchProvider(
      'gitlab-vscode-ide',
      new GitLabFileSearchProvider(new FileSearcher(fileList), result.repoRoot),
    );

    // TODO: We used to include the Spike implementation of SourceControl (inspired from Git).
    //       We've removed this since we moved over to the BrowserFS implementation of the FS.
    //       Look at commit 7f00b4f to view the old `git/` modules.
    // setupSCM(fs, git);

    // Refresh file view...
    await Promise.allSettled([
      vscode.commands.executeCommand('workbench.action.closeSidebar'),
      vscode.commands.executeCommand('workbench.explorer.fileView.focus'),
    ]);

    await ready();
  };

export async function activate(context: vscode.ExtensionContext) {
  // eslint-disable-next-line @typescript-eslint/no-floating-promises
  vscode.commands.executeCommand('workbench.action.closeAllEditors');

  await vscode.window.withProgress(
    {
      cancellable: false,
      location: vscode.ProgressLocation.Notification,
      title: 'Initializing GitLab Web IDE...',
    },
    fetchContextTask(context),
  );

  await vscode.window.showWarningMessage('The Web IDE is under development', {
    modal: true,
    detail:
      'Have fun previewing this feature, but keep in mind that any changes made through this UI might be lost.',
  });
}

// eslint-disable-next-line @typescript-eslint/no-empty-function
export function deactivate() {}
