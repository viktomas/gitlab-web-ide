import {
  CancellationToken,
  FileSearchOptions,
  FileSearchProvider,
  FileSearchQuery,
  Uri,
} from 'vscode';
import { IFileSearcher } from '../types';

export class GitLabFileSearchProvider implements FileSearchProvider {
  private readonly _searcher: IFileSearcher;

  private readonly _repoRootPath: string;

  constructor(searcher: IFileSearcher, repoRootPath: string) {
    this._searcher = searcher;
    this._repoRootPath = repoRootPath;
  }

  async provideFileSearchResults(
    query: FileSearchQuery,
    options: FileSearchOptions,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    token: CancellationToken,
  ): Promise<Uri[]> {
    if (!query.pattern) {
      return [];
    }

    // TODO: What about the other options???
    const result = await this._searcher.searchBlobPaths(query.pattern, options.maxResults);

    return result.map(x => Uri.joinPath(Uri.parse(`gitlab-vscode-ide:/`), this._repoRootPath, x));
  }
}
