import { GitLabClient } from '@gitlab/gitlab-api-client';
import { IFullConfig, StartCommandResponse } from './types';

export const handleStartCommand = async (
  config: IFullConfig,
  client: GitLabClient,
): Promise<StartCommandResponse> => {
  const branch = await client.fetchProjectBranch(config.projectPath, config.ref);

  const tree = await client.fetchTree(config.projectPath, branch.commit.id);

  const blobs = tree.filter(item => item.type === 'blob');

  return {
    branch,
    files: blobs,
    repoRoot: config.repoRoot,
  };
};
