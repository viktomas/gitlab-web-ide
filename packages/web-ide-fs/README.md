# @gitlab/web-ide-fs

This package is responsible for providing the types and default implementation for
the FileSystem and SourceControl that power the Web IDE.
