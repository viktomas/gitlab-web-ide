import { FileList } from './FileList';
import { createSystems } from './create';
import { IFileSystem } from './types';
import { REPO_ROOT, DEFAULT_FILES, DEFAULT_FILE_ARRAY } from '../test-utils/fs';
import { FakeFileContentProvider } from '../test-utils';

describe('FileList', () => {
  let subject: FileList;
  let fs: IFileSystem;

  describe('default', () => {
    beforeEach(async () => {
      ({ fs } = await createSystems({
        contentProvider: new FakeFileContentProvider(DEFAULT_FILES),
        gitLsTree: DEFAULT_FILE_ARRAY,
        repoRoot: REPO_ROOT,
      }));

      subject = new FileList(fs, REPO_ROOT);
    });

    describe('listAllBlobs', () => {
      it('lists all blobs', async () => {
        const result = await subject.listAllBlobs();

        // Order doesn't matter
        expect(result.sort()).toEqual(Object.keys(DEFAULT_FILES).sort());
      });
    });
  });
});
