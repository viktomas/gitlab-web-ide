import { FileList } from './FileList';
import { FileListWithCache } from './FileListWithCache';
import { FakeFileContentProvider } from '../test-utils/FakeFileContentProvider';
import { DEFAULT_FILES, DEFAULT_FILE_ARRAY, REPO_ROOT } from '../test-utils/fs';
import { createSystems } from './create';
import { IFileSystem } from './types';

describe('FileListWithCache', () => {
  let fs: IFileSystem;
  let fileList: FileList;
  let subject: FileListWithCache;

  describe('default', () => {
    beforeEach(async () => {
      ({ fs } = await createSystems({
        contentProvider: new FakeFileContentProvider(DEFAULT_FILES),
        gitLsTree: DEFAULT_FILE_ARRAY,
        repoRoot: REPO_ROOT,
      }));

      fileList = new FileList(fs, REPO_ROOT);
      subject = new FileListWithCache(fileList, fs);
    });

    describe('listAllBlobs', () => {
      it('returns result from base fileList', async () => {
        const actual = await subject.listAllBlobs();
        const expected = await fileList.listAllBlobs();

        expect(actual).toEqual(expected);
      });

      it('caches', async () => {
        jest.spyOn(fileList, 'listAllBlobs');

        const original = await subject.listAllBlobs();
        const final = await subject.listAllBlobs();

        expect(fileList.listAllBlobs).toHaveBeenCalledTimes(1);
        expect(original).toBe(final);
      });

      it.each`
        desc           | act
        ${'writeFile'} | ${() => fs.writeFile(`/${REPO_ROOT}/README.md`, new TextEncoder().encode('TEST'))}
        ${'rm'}        | ${() => fs.rm(`/${REPO_ROOT}/README.md`, { recursive: true })}
      `('invalidates cache when $desc', async ({ act }: { act: () => Promise<void> }) => {
        jest.spyOn(fileList, 'listAllBlobs');

        await subject.listAllBlobs();

        await act();

        // Time to invalidate with a fs update!
        await subject.listAllBlobs();

        expect(fileList.listAllBlobs).toHaveBeenCalledTimes(2);
      });
    });
  });
});
