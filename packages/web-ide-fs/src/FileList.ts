import { FileListWithCache } from './FileListWithCache';
import { FileType, IFileList, IFileSystem } from './types';
import { joinPaths } from './utils';

/**
 * This provides an unordered list of all blobs from the given "fs" relative to the "repoPath"
 */
export class FileList implements IFileList {
  private readonly _fs: IFileSystem;

  private readonly _repoPath: string;

  constructor(fs: IFileSystem, repoPath: string) {
    this._fs = fs;
    this._repoPath = repoPath;
  }

  async listAllBlobs(): Promise<string[]> {
    return this._listAllBlobs(joinPaths('/', this._repoPath));
  }

  withCache(): IFileList {
    return new FileListWithCache(this, this._fs);
  }

  private async _listAllBlobs(actualPath: string, relativePath = ''): Promise<string[]> {
    const children = await this._fs.readdirWithTypes(actualPath);

    const childResults = await Promise.all(
      children.map(async ([childName, type]) => {
        const childActualPath = joinPaths(actualPath, childName);
        const childRelPath = joinPaths(relativePath, childName);

        if (type === FileType.Blob) {
          return [childRelPath];
        }
        return this._listAllBlobs(childActualPath, childRelPath);
      }),
    );

    return childResults.flatMap(x => x);
  }
}
