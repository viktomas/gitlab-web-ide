// >>> Enums
export enum FileType {
  Blob = 1,
  Tree = 2,
}

export enum FileStatusType {
  Modified = 1,
  Created = 2,
  Deleted = 3,
}

// =========================
// >>> Interfaces (main)
export interface IFileStats {
  readonly mode: number;
  readonly ctime: number;
  readonly mtime: number;
  readonly size: number;
  readonly type: FileType;
}

export interface IGitLsTreeEntry {
  readonly path: string;
  readonly mode: string;
}

export interface ISourceControlFileStatus {
  readonly path: string;
  readonly type: FileStatusType;
}

/**
 * Responsible for providing a friendly read/write FileSystem
 *
 * We want clients of web-ide-fs to depend on this interface so
 * that we are less coupled to implementation details (like BrowserFS)
 * and the interactions are simpler.
 */
export interface IFileSystem {
  stat(path: string): Promise<IFileStats>;

  readdir(path: string): Promise<string[]>;

  readdirWithTypes(path: string): Promise<[string, FileType][]>;

  mkdir(path: string): Promise<void>;

  readFile(path: string): Promise<Uint8Array>;

  writeFile(path: string, data: Uint8Array): Promise<void>;

  // TODO: Remove opt: { recursive }. It isn't being used...
  rm(path: string, opt: { recursive: boolean }): Promise<void>;

  rename(oldPath: string, newPath: string): Promise<void>;

  // THOUGHT: We might want to abstract this out of this type...
  lastModifiedTime(): Promise<number>;
}

/**
 * Responsible for viewing the Source Control state for the Web IDE's File System
 */
export interface ISourceControl {
  status(): ISourceControlFileStatus[];
}

/**
 * Responsible for providing file content based on the given path
 */
export interface IFileContentProvider {
  getContent(path: string): Promise<Uint8Array>;
}

export interface ISystems {
  fs: IFileSystem;
  // fsModifiedTimeProvider: IFileSystemTimeProvider;
  sourceControl: ISourceControl;
}

export interface IFileList {
  listAllBlobs(): Promise<string[]>;
}
