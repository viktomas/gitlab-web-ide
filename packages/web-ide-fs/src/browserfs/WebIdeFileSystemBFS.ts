import { FileFlag } from 'browserfs/dist/node/core/file_flag';
import { FileSystem as BrowserFSFileSystem } from 'browserfs/dist/node/core/file_system';
import Stats from 'browserfs/dist/node/core/node_fs_stats';
import { FileType, IFileStats, IFileSystem } from '../types';
import { joinPaths } from '../utils';
import { FileSystemPromiseAdapter } from './FileSystemPromiseAdapter';

const convertToFileType = (stat: Stats): FileType => {
  if (stat.isDirectory()) {
    return FileType.Tree;
  }
  return FileType.Blob;
};

const convertToFileStats = (stat: Stats): IFileStats => ({
  ctime: stat.ctime.getTime(),
  mtime: stat.mtime.getTime(),
  size: stat.size,
  mode: stat.mode,
  type: convertToFileType(stat),
});

export interface WebIdeFileSystemBFSOptions {
  // This is the main OverlayFS
  fs: BrowserFSFileSystem;

  // This is the writableFS we need to calculate The-Real-Modified-Time TM
  writableFS: BrowserFSFileSystem;

  // Repo root path is needed to calculate The-Real-Modified-Time TM
  repoRootPath: string;
}

export class WebIdeFileSystemBFS implements IFileSystem {
  private readonly _fs: FileSystemPromiseAdapter;

  private readonly _writableFS: FileSystemPromiseAdapter;

  private readonly _repoRootPath: string;

  constructor({ fs, writableFS, repoRootPath }: WebIdeFileSystemBFSOptions) {
    this._fs = new FileSystemPromiseAdapter(fs);
    this._writableFS = new FileSystemPromiseAdapter(writableFS);
    this._repoRootPath = repoRootPath;
  }

  async lastModifiedTime(): Promise<number> {
    const repoRootStat = await this._fs.stat(joinPaths('/', this._repoRootPath), false);
    const repoMTime = repoRootStat.mtime.getTime();
    const deletedMTime = await this._deletedFilesModifiedTime();

    return Math.max(repoMTime, deletedMTime);
  }

  private async _deletedFilesModifiedTime(): Promise<number> {
    const hasDeletedFiles = await this._writableFS.exists('/.deletedFiles.log');

    if (!hasDeletedFiles) {
      return -1;
    }

    const stat = await this._writableFS.stat('/.deletedFiles.log', false);

    return stat.mtime.getTime();
  }

  async stat(path: string): Promise<IFileStats> {
    const stat = await this._fs.stat(path, false);

    return convertToFileStats(stat);
  }

  readdir(path: string): Promise<string[]> {
    return this._fs.readdir(path);
  }

  async readdirWithTypes(path: string): Promise<[string, FileType][]> {
    const children = await this._fs.readdir(path);

    const result: Promise<[string, FileType]>[] = children.map(name => {
      const childPath = joinPaths(path, name);

      return this.stat(childPath).then(stat => [name, stat.type]);
    });

    return Promise.all(result);
  }

  mkdir(path: string): Promise<void> {
    // TODO: Is 0 for the mode really safe?
    return this._fs.mkdir(path, 0);
  }

  async readFile(path: string): Promise<Uint8Array> {
    const content = await this._fs.readFile(path, null, FileFlag.getFileFlag('r'));

    // We know this will be a Buffer because we passed "null" for "utf-8"
    return <Buffer>content;
  }

  writeFile(path: string, data: Uint8Array): Promise<void> {
    return this._fs.writeFile(path, Buffer.from(data), null, FileFlag.getFileFlag('w'), 0);
  }

  rename(oldPath: string, newPath: string): Promise<void> {
    return this._fs.rename(oldPath, newPath);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async rm(path: string, opt: { recursive: boolean }): Promise<void> {
    const stat = await this.stat(path);

    return this._rmrf(path, stat.type);
  }

  // eslint-disable-next-line consistent-return
  private async _rmrf(path: string, fileType: FileType): Promise<void> {
    if (fileType === FileType.Blob) {
      return this._removeFile(path);
    }

    const children = await this.readdirWithTypes(path);
    const childrenRemoved = children.map(([name, type]) => {
      const childPath = joinPaths(path, name);

      return this._rmrf(childPath, type);
    });
    await Promise.all(childrenRemoved);

    await this._removeDir(path);
  }

  private _removeDir(path: string): Promise<void> {
    return this._fs.rmdir(path);
  }

  private _removeFile(path: string): Promise<void> {
    return this._fs.unlink(path);
  }
}
