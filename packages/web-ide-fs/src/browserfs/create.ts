import { FileSystem } from 'browserfs';
import InMemoryFileSystem from 'browserfs/dist/node/backend/InMemory';
import OverlayFS from 'browserfs/dist/node/backend/OverlayFS';
import { IFileContentProvider, IGitLsTreeEntry } from '../types';
import { createFileEntryMap } from '../utils';
import { GitLabReadableFileSystem } from './GitLabReadableFileSystem';
import { initializeEnvironment } from './initializeEnvironment';
import { createAsPromise } from './createAsPromise';

export interface ICreateBrowserFSOptions {
  gitLsTree: IGitLsTreeEntry[];
  contentProvider: IFileContentProvider;
  repoRoot: string;
}

export interface ICreateBrowserFSReturn {
  fs: OverlayFS;
  readable: GitLabReadableFileSystem;
  writable: InMemoryFileSystem;
}

export const create = async (options: ICreateBrowserFSOptions): Promise<ICreateBrowserFSReturn> => {
  // The BrowserFS modules only work when the environment is initialized.
  // This initialization should be memoized.
  initializeEnvironment();

  const readable = await createAsPromise(GitLabReadableFileSystem.Create, {
    entries: createFileEntryMap(options.gitLsTree, options.repoRoot),
    contentProvider: options.contentProvider,
  });

  const writable = await createAsPromise(FileSystem.InMemory.Create, {});

  // BrowserFS file system
  const fs = await createAsPromise(FileSystem.OverlayFS.Create, {
    readable,
    writable,
  });

  return {
    fs,
    readable,
    writable,
  };
};
