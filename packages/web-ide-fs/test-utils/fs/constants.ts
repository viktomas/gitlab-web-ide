export const REPO_ROOT = 'test-repo';

export const DEFAULT_FILE_ARRAY = [
  {
    path: 'README.md',
    content: 'Lorem ipsum dolar sit\namit\n\n# Title\n123456\n',
    mode: '100644',
  },
  {
    path: 'foo/bar/index.js',
    content: 'console.log("Hello world!")\n',
    mode: '100655',
  },
  {
    path: 'foo/README.md',
    content: '# foo\n\nIt has foos.\n',
    mode: '155555',
  },
];

export const DEFAULT_FILES: Record<string, string> = DEFAULT_FILE_ARRAY.reduce(
  (acc, x) => Object.assign(acc, { [x.path]: x.content }),
  {},
);
