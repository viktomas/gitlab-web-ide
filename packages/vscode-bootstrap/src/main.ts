import { Config } from '@gitlab/web-ide-types';
import { getConfigFromDOM } from './utils/getConfigFromDOM';
import { start } from './start';
import { insertScript } from './utils/insertScript';
import { insertMeta } from './utils/insertMeta';

const SCRIPT_VSCODE_LOADER = 'vscode/out/vs/loader.js';
const SCRIPT_VSCODE_WEB_PACKAGE_PATHS = 'vscode/out/vs/webPackagePaths.js';
const SCRIPT_VSCODE_WORKBENCH_NLS = 'vscode/out/vs/workbench/workbench.web.main.nls.js';
const SCRIPT_VSCODE_WORKBENCH = 'vscode/out/vs/workbench/workbench.web.main.js';

// initialized by loading SCRIPT_VSCODE_LOADER
// eslint-disable-next-line @typescript-eslint/no-explicit-any
declare const define: any;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
declare const require: any;

declare global {
  interface Window {
    // initialized by loading SCRIPT_VSCODE_LOADER
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    trustedTypes: any;

    // initialized by loading SCRIPT_VSCODE_WEB_PACKAGE_PATHS
    webPackagePaths: Record<string, string>;
  }
}

const setupBuiltinExtensionMeta = async (config: Config) => {
  const extensionPackageJSONUrl = `${config.baseUrl}/vscode/extensions/gitlab-web-ide/package.json`;

  const rawJson = await fetch(extensionPackageJSONUrl).then(x => x.text());

  const packageJSON = JSON.parse(rawJson);

  insertMeta('gitlab-builtin-vscode-extensions', [
    {
      extensionPath: 'gitlab-web-ide',
      packageJSON,
    },
  ]);
};

/**
 * This makes sure that the navigator keyboard is compatible
 *
 * VSCode reads from this global sometimes and was throwing some errors... This might not be needed...
 */
const setupNavigatorKeyboard = () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  if ((navigator as any).keyboard) {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    Object.assign((navigator as any).keyboard, {
      getLayoutMap: () => Promise.resolve(new Map()),
    });
  }
};

const setupAMDRequire = (config: Config) => {
  const vscodeUrl = `${config.baseUrl}/vscode`;

  // eslint-disable-next-line no-restricted-globals, array-callback-return, prefer-arrow-callback, func-names
  Object.keys(self.webPackagePaths).map(function (key) {
    // eslint-disable-next-line no-restricted-globals
    self.webPackagePaths[
      key
      // eslint-disable-next-line no-restricted-globals
    ] = `${vscodeUrl}/bundled_node_modules/${key}/${self.webPackagePaths[key]}`;
  });

  require.config({
    baseUrl: `${vscodeUrl}/out`,
    recordStats: true,
    trustedTypesPolicy: window.trustedTypes?.createPolicy('amdLoader', {
      createScriptURL(value: string) {
        if (value.startsWith(vscodeUrl)) {
          return value;
        }
        throw new Error(`Invalid script url: ${value}`);
      },
    }),
    // eslint-disable-next-line no-restricted-globals
    paths: self.webPackagePaths,
  });

  // Void these features
  [
    'vs/workbench/contrib/welcome/gettingStarted/browser/gettingStarted.contribution',
    'vs/workbench/contrib/welcome/page/browser/welcomePage.contribution',
  ].forEach(path => {
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    define(path, () => {});
  });
};

const main = async () => {
  const config = getConfigFromDOM();

  await setupBuiltinExtensionMeta(config);

  setupNavigatorKeyboard();

  await Promise.all([
    insertScript(`${config.baseUrl}/${SCRIPT_VSCODE_LOADER}`, config.nonce),
    insertScript(`${config.baseUrl}/${SCRIPT_VSCODE_WEB_PACKAGE_PATHS}`, config.nonce),
  ]);

  setupAMDRequire(config);

  await Promise.all([
    insertScript(`${config.baseUrl}/${SCRIPT_VSCODE_WORKBENCH_NLS}`, config.nonce),
    insertScript(`${config.baseUrl}/${SCRIPT_VSCODE_WORKBENCH}`, config.nonce),
  ]);

  start(config);
};

main().catch(e => {
  // TODO: Lets do something nicer...
  // eslint-disable-next-line no-console
  console.error(e);
  // eslint-disable-next-line no-alert
  alert('An unexpected error occurred! Please open the developer console for details.');
});
