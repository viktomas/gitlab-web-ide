import { Config } from '@gitlab/web-ide-types';
import { createCommands } from '@gitlab/vscode-mediator-commands';
import { createBufferWrapper } from './utils/createBufferWrapper';
import { getRepoRoot } from './utils/getRepoRoot';

// amd is real yo
// eslint-disable-next-line @typescript-eslint/no-explicit-any
declare const define: any;

export const start = (config: Config) => {
  define('gitlab-vscode/main', [
    'vs/workbench/workbench.web.main',
    'vs/base/common/buffer',
    'vs/platform/log/common/log',
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
  ], (workbench: any, bufferModule: any, commonLogModule: any) => {
    const repoRoot = getRepoRoot(config.projectPath);
    const fullConfig = {
      ...config,
      repoRoot,
    };

    // example https://sourcegraph.com/github.com/microsoft/vscode@12aa93aacd80e844464b33dcac055c4c29f6d4e1/-/blob/src/vs/code/browser/workbench/workbench.ts?L521:2#tab=def
    workbench.create(document.body, {
      // what: Flag the gitlab-web-ide extension as an environment extension which cannot be disabled
      // https://gitlab.com/gitlab-org/gitlab-web-ide/-/issues/13#note_1053126388
      // https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/blob/fa3eb589de07ab4db0500c32519ce41940c11241/src/vs/workbench/browser/web.api.ts#L215
      enabledExtensions: ['gitlab.gitlab-web-ide'],
      developmentOptions: {
        // TODO: Make logLevel configurable
        logLevel: commonLogModule.parseLogLevel('trace'),
      },
      homeIndicator: {
        href: 'https://gitlab.com',
        icon: 'code',
        title: 'GitLab',
      },
      // TODO - Maybe we want this...
      welcomeBanner: undefined,
      commands: createCommands(fullConfig, createBufferWrapper(bufferModule)),
      defaultLayout: {
        views: [],
        editors: [],
        force: true,
      },
      settingsSyncOptions: {
        enabled: false,
      },
      productConfiguration: {
        // https://sourcegraph.com/github.com/sourcegraph/openvscode-server@3169b2e0423a56afba4fa1c824f966e7b3b9bf07/-/blob/product.json?L586
        // Gotta use open vsx! Otherwise we run into CORS issues with vscode :|
        extensionsGallery: {
          serviceUrl: 'https://open-vsx.org/vscode/gallery',
          itemUrl: 'https://open-vsx.org/vscode/item',
          resourceUrlTemplate:
            'https://open-vsx.org/vscode/asset/{publisher}/{name}/{version}/Microsoft.VisualStudio.Code.WebResources/{path}',
          controlUrl: '',
          recommendationsUrl: '',
        },
      },
      // This is needed so that we don't enter multiple workspace zone :|
      workspaceProvider: {
        workspace: { folderUri: workbench.URI.parse(`gitlab-vscode-ide:///${repoRoot}`) },
        trusted: true,
        async open() {
          return false;
        },
      },
    });
  });
};
