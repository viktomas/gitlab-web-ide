export const createError = (msg: string) => new Error(`[gitlab-vscode] ${msg}`);
