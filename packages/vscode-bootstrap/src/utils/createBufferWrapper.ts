import { VSBufferWrapper } from '@gitlab/vscode-mediator-commands';

// eslint-disable-next-line @typescript-eslint/no-explicit-any, arrow-body-style
export const createBufferWrapper = (bufferModule: any): VSBufferWrapper => {
  return (actual: Uint8Array) => bufferModule.VSBuffer.wrap(actual);
};
