import { Config } from '@gitlab/web-ide-types';
import webIdeExtensionMeta from '@gitlab/web-ide-vscode-extension/vscode.package.json';
import { useMockAMDEnvironment } from '../test-utils/amd';
import { start } from './start';

const TEST_CONFIG: Config = {
  baseUrl: 'https://test.example.com',
  gitlabToken: 'secrettoken',
  gitlabUrl: 'https://test.example-gitlab.com',
  projectPath: 'my-group/lorem',
  ref: 'abcdef123',
};

describe('vscode-bootstrap start', () => {
  const amd = useMockAMDEnvironment();

  const workbenchModule = {
    create: jest.fn(),
    URI: {
      parse: (x: string) => `URI.parse-${x}`,
    },
  };
  const bufferModule = {};
  const commonLogModule = {
    parseLogLevel: (x: string) => `parseLogLevel-${x}`,
  };

  beforeAll(() => {
    amd.shim();
  });

  beforeEach(() => {
    amd.define('vs/workbench/workbench.web.main', () => workbenchModule);
    amd.define('vs/base/common/buffer', () => bufferModule);
    amd.define('vs/platform/log/common/log', () => commonLogModule);
  });

  afterEach(() => {
    amd.cleanup();
  });

  describe('default', () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let subject: any;

    beforeEach(() => {
      start(TEST_CONFIG);

      expect(workbenchModule.create).toHaveBeenCalledTimes(1);

      [[, subject]] = workbenchModule.create.mock.calls;
    });

    it('creates workbench on body', () => {
      expect(workbenchModule.create).toHaveBeenCalledWith(document.body, subject);
    });

    it('creates workbench with enabledExtensions', () => {
      const { publisher, name } = webIdeExtensionMeta;

      expect(subject).toMatchObject({
        enabledExtensions: [`${publisher}.${name}`],
      });
    });
  });
});
