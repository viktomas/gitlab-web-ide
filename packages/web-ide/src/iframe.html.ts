export default `<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <!-- Disable pinch zooming -->
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no"
    />
    <meta id="gl-config-json" data-settings="{{json}}" />
    <!--<link rel="manifest" href="{{baseUrl}}/vscode/manifest.json" />-->
    <link
      data-name="vs/workbench/workbench.web.main"
      rel="stylesheet"
      href="{{baseUrl}}/vscode/out/vs/workbench/workbench.web.main.css"
    />
  </head>
  <body>
    <script src="{{baseUrl}}/main.js" nonce="{{nonce}}"></script>
  </body>
</html>`;
