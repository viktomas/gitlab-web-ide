import Mustache from 'mustache';
// eslint-disable-next-line import/no-extraneous-dependencies
import { Config } from '@gitlab/web-ide-types';

// We have to move in-and-out of directory so that import stays the same after tsc into lib/
import iframeHtml from './iframe.html';

export const createError = (msg: string) => new Error(`[gitlab-vscode] ${msg}`);

const getIframeHtml = (config: Config) =>
  Mustache.render(iframeHtml, {
    ...config,
    json: JSON.stringify(config),
  });

const waitForReady = (iframe: HTMLIFrameElement): Promise<void> => {
  const { contentWindow } = iframe;

  if (!contentWindow) {
    throw createError('Could not find contentWindow for iframe.');
  }

  return new Promise<void>(resolve => {
    const listener = (e: MessageEvent) => {
      if (e.data?.key === 'ready') {
        contentWindow.removeEventListener('message', listener);
        resolve();
      }
    };

    contentWindow.addEventListener('message', listener);
  });
};

export const start = (el: Element, config: Config): Promise<void> => {
  const iframe = document.createElement('iframe');
  Object.assign(iframe.style, {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    width: '100%',
    height: '100%',
    border: 'none',
    margin: 0,
    padding: 0,
  });
  el.appendChild(iframe);

  if (!iframe.contentWindow) {
    throw createError('Could not find contentWindow for iframe.');
  }

  iframe.contentWindow.document.open();
  iframe.contentWindow.document.write(getIframeHtml(config));
  iframe.contentWindow.document.close();

  return waitForReady(iframe);
};
