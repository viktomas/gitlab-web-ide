const { pnpPlugin } = require('@yarnpkg/esbuild-plugin-pnp');
const { NodeModulesPolyfillPlugin } = require('@esbuild-plugins/node-modules-polyfill');
const esbuild = require('esbuild');

module.exports = options =>
  esbuild
    .build({
      bundle: true,
      plugins: [NodeModulesPolyfillPlugin(), pnpPlugin()],
      loader: {
        '.html': 'text',
      },
      external: ['vscode'],
      ...options,
    })
    .catch(() => process.exit(1));
