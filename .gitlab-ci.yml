stages:
  - prepare
  - build-and-test
  - publish-and-deploy

# NOTE: A lot of this is inspired from:
#   - https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/1a362833aae9953e9cc927f9ba4c06ce99483bbc/.gitlab-ci.yml
default:
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:gitlab-vscode-nodeless
  tags:
    - gitlab-org
  interruptible: true # All jobs are interruptible by default
  retry:
    max: 2 # This is confusing but this means "3 runs at max".
    when:
      - unknown_failure
      - api_failure
      - runner_system_failure
      - job_execution_timeout
      - stuck_or_timeout_failure

variables:
  ### PERFORMANCE ###
  # GIT_* variables to speed up repo cloning/fetching
  GIT_DEPTH: '10'
  GIT_STRATEGY: 'fetch'
  GIT_SUBMODULE_STRATEGY: 'none'
  # Disabling LFS speeds up jobs, because runners don't have to perform the LFS steps during repo clone/fetch
  GIT_LFS_SKIP_SMUDGE: '1'
  # NO_CONTRACTS speeds up middleman builds
  NO_CONTRACTS: 'true'

  ### RELIABILITY ###
  # Reduce flaky builds via https://docs.gitlab.com/ee/ci/runners/configure_runners.html#job-stages-attempts variables
  GET_SOURCES_ATTEMPTS: '3'
  ARTIFACT_DOWNLOAD_ATTEMPTS: '3'
  RESTORE_CACHE_ATTEMPTS: '3'
  EXECUTOR_JOB_SECTION_ATTEMPTS: '3'
  # Performs an error check after each Bash script command is executed, and exits if the previously executed command returned a non-zero exit code
  # https://gitlab.com/gitlab-org/gitlab-runner/-/merge_requests/2671
  FF_ENABLE_BASH_EXIT_CODE_CHECK: 'true'
  GITLAB_WEB_IDE_PROJECT_ID: '35104827'

# main branch in the original repo, NOT a main branch MR for a forked repo.
# NOTE: We must make sure to exclude forked repo MRs by checking `CI_MERGE_REQUEST_SOURCE_PROJECT_ID is null,
# because the pipeline might be in an MR against a fork's 'main' branch, and in this case this rule
# would incorrectly trigger when the "Pipeline for Merged Results" is run for the MR.
.if-main-original-repo: &if-main-original-repo
  if: '$CI_COMMIT_REF_NAME == "main" && $CI_PROJECT_ID == $GITLAB_WEB_IDE_PROJECT_ID && $CI_MERGE_REQUEST_SOURCE_PROJECT_ID == null'

# merge request, ONLY for a branch in the original repo
.if-merge-request-original-repo: &if-merge-request-original-repo
  if: '$CI_MERGE_REQUEST_IID && $CI_MERGE_REQUEST_SOURCE_PROJECT_ID == $GITLAB_WEB_IDE_PROJECT_ID'

# merge request, ONLY for a branch in a forked repo
.if-merge-request-forked-repo: &if-merge-request-forked-repo
  if: '$CI_MERGE_REQUEST_IID && $CI_MERGE_REQUEST_SOURCE_PROJECT_ID != $GITLAB_WEB_IDE_PROJECT_ID'

# "always" - main branch in the original repo, OR any merge request
.if-main-original-repo-or-merge-request: &if-main-original-repo-or-merge-request
  if: '($CI_COMMIT_REF_NAME == "main" && $CI_PROJECT_ID == $GITLAB_WEB_IDE_PROJECT_ID) || $CI_MERGE_REQUEST_IID'

.dependency-cache:
  cache:
    key: 'gitlab-web-ide-dependency-cache'
    policy: pull
    paths:
      - .yarn/cache
      # cache node and yarn distributions to avoid downloading them again
      - '${PWD}/tmp/${GITLAB_NODE_DIST}/**/*'
      - '${PWD}/tmp/${GITLAB_YARN_DIST}/**/*'

.node-and-yarn:
  extends: .dependency-cache
  variables:
    GITLAB_NODE_VERSION: 16.15.0
    GITLAB_NODE_DIST: node-v${GITLAB_NODE_VERSION}-linux-x64
    GITLAB_YARN_VERSION: 1.22.17
    GITLAB_YARN_DIST: yarn-v${GITLAB_YARN_VERSION}
  before_script:
    # download node distribution if not cached
    - if [ ! -d tmp/${GITLAB_NODE_DIST} ]; then curl -L https://nodejs.org/dist/v${GITLAB_NODE_VERSION}/${GITLAB_NODE_DIST}.tar.gz | tar -C tmp -zxf -; fi
    # add node to path
    - echo "export PATH=\${PATH}:${PWD}/tmp/${GITLAB_NODE_DIST}/bin" > 10-add-node-to-path.sh
    - sudo cp 10-add-node-to-path.sh /etc/profile.d/10-add-node-to-path.sh
    - source /etc/profile.d/10-add-node-to-path.sh
    - node --version
    # download yarn distribution if not cached
    - if [ ! -d tmp/${GITLAB_YARN_DIST} ]; then curl -L https://github.com/yarnpkg/yarn/releases/download/v${GITLAB_YARN_VERSION}/${GITLAB_YARN_DIST}.tar.gz | tar -C tmp -zxf -; fi
    # add yarn to path
    - echo "export PATH=\${PATH}:${PWD}/tmp/${GITLAB_YARN_DIST}/bin" > 20-add-yarn-to-path.sh
    - sudo cp 20-add-yarn-to-path.sh /etc/profile.d/20-add-yarn-to-path.sh
    - source /etc/profile.d/20-add-yarn-to-path.sh
    - yarn --version
    # yarn install
    - yarn install --immutable

###################################
#
# PREPARE STAGE
#
###################################

# Prevents pipeline from being interrupted by subsequent commits.
# Pipelines on the master branch of the original repo are never interruptible.
# MR pipelines (on original repo and forks) may be made non-interruptible by manually running the job.
dont-interrupt-me:
  image: alpine:edge
  interruptible: false
  stage: prepare
  rules:
    - <<: *if-main-original-repo
      when: manual
      allow_failure: true
    - <<: *if-merge-request-original-repo
      when: manual
      allow_failure: true
    - <<: *if-merge-request-forked-repo
      when: manual
      allow_failure: true
  variables:
    GIT_STRATEGY: none
  script:
    - echo "# This job makes sure this pipeline won't be interrupted on master. It can also be triggered manually to prevent a pipeline from being interrupted. See https://docs.gitlab.com/ee/ci/yaml/#interruptible."

# This is a manual job for debugging any unexpected behavior encountered while refactoring the CI config
expose-ci-rules-variables:
  image: alpine:edge
  stage: prepare
  rules:
    - <<: *if-main-original-repo-or-merge-request
      when: manual
      allow_failure: true
  variables:
    GIT_STRATEGY: none
  script:
    - echo "CI_COMMIT_REF_NAME = ${CI_COMMIT_REF_NAME}"
    - echo "CI_PROJECT_ID = ${CI_PROJECT_ID}"
    - echo "CI_MERGE_REQUEST_IID = ${CI_MERGE_REQUEST_IID}"
    - echo "CI_MERGE_REQUEST_TITLE = ${CI_MERGE_REQUEST_TITLE}"
    - echo "CI_MERGE_REQUEST_SOURCE_PROJECT_ID = ${CI_MERGE_REQUEST_SOURCE_PROJECT_ID}"
    - echo "CI_COMMIT_REF_SLUG = ${CI_COMMIT_REF_SLUG}"
    - echo "CI_PIPELINE_SOURCE = ${CI_PIPELINE_SOURCE}"

# Only push the cache from this job, to save time on all other jobs.
push-cache:
  extends: .node-and-yarn
  stage: prepare
  rules:
    - <<: *if-main-original-repo
    # NOTE: Uncomment this line if you are working on changes to caching in a Merge Request,
    #       so that this job will be run for MRs as well. Normally, this line should remain
    #       commented out, to speed up MR pipelines (per the job comment above)
    # - <<: *if-merge-request-original-repo
  cache:
    policy: pull-push
  script:
    - echo "Pushing updated cache..."

###################################
#
# BUILD AND TEST STAGE
#
###################################

create-development-package:
  extends: .node-and-yarn
  stage: build-and-test
  variables:
    # why: `yarn version` needs access to the commit history
    # https://yarnpkg.com/features/release-workflow#caveat
    # https://gitlab.com/gitlab-org/gitlab-web-ide/-/jobs/2842454123#L6673
    GIT_DEPTH: 0
  rules:
    - <<: *if-main-original-repo-or-merge-request
      when: manual
      allow_failure: true
  script:
    # why: we need to add to safe.directory because `yarn version` will trigger `git` commands
    # https://yarnpkg.com/features/release-workflow#caveat
    - git config --global --add safe.directory $PWD
    - ./scripts/pack-web-ide-package.sh
  artifacts:
    paths:
      - tmp/packages

build:
  extends: .node-and-yarn
  stage: build-and-test
  rules:
    - <<: *if-main-original-repo-or-merge-request
  needs: []
  script:
    - yarn run build:example
  artifacts:
    paths:
      - packages/example/dist

lint:
  extends: .node-and-yarn
  stage: build-and-test
  needs: []
  rules:
    - <<: *if-main-original-repo-or-merge-request
  script:
    - yarn run lint

jest:
  extends: .node-and-yarn
  stage: build-and-test
  needs: []
  rules:
    - <<: *if-main-original-repo-or-merge-request
  script:
    - yarn test

###################################
#
# PUBLISH AND DEPLOY STAGE
#
###################################

# TODO: Add support for publishing package to npm: https://gitlab.com/gitlab-org/gitlab-web-ide/-/issues/3

publish-development-package:
  extends: .node-and-yarn
  stage: publish-and-deploy
  needs:
    - job: create-development-package
      artifacts: true
  rules:
    - <<: *if-main-original-repo
      when: manual
      allow_failure: true
  script:
    - export GITLAB_WEB_IDE_TAR_PATH="$(find tmp/packages -name "gitlab-web-ide*.tgz" -type f)"
    - npm config set //registry.npmjs.org/:_authToken $NPM_TOKEN
    - npm publish --access public $GITLAB_WEB_IDE_TAR_PATH

pages:
  extends: .node-and-yarn
  stage: publish-and-deploy
  rules:
    # This job cannot be run on forks, only the original repo.
    - <<: *if-main-original-repo
    # NOTE: Pages does not yet support review apps, so we can't yet deploy from MRs. We could
    #       switch to a non-pages based deploy if we want (like www-gitlab-com) to
    #       support review apps.
    # - <<: *if-merge-request-original-repo
  script:
    - mv packages/example/dist public
  artifacts:
    paths:
      - public
